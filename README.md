# C33s Form Extra Bundle

currently only consisting of:

- Html5 No-Validation Extension from https://stackoverflow.com/a/33827900.
- CustomTypeGuesser for VichImageType and VichFileType
- SortableEntityType


## Compatibility 

- Branch 1.x is for Symfony 2.8 - 3.2 (maybe even lower symfony versions. haven't tested it)
- Branch 2.x is for Symfony >= 3.3


## Installation

- `composer require c33s/form-extra-bundle`
add the following, according your directory structure, to your project: 
    
- Symfony3: `new C33s\Bundle\FormExtraBundle\C33sFormExtraBundle(),` to `bundles.php` in the `['dev', 'test']` section
- Symfony4: `C33s\Bundle\FormExtraBundle\C33sFormExtraBundle::class => ['dev' => true, 'test' => true],` to `bundles.php`
