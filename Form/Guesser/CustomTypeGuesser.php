<?php

namespace C33s\Bundle\FormExtraBundle\Form\Guesser;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser;
use Symfony\Component\Form\Guess\Guess;
use Symfony\Component\Form\Guess\TypeGuess;

class CustomTypeGuesser extends DoctrineOrmTypeGuesser
{
    /**
     * {@inheritdoc}
     */
    public function guessType($class, $property)
    {
        if (null !== $metadataAndName = $this->getMetadata($class)) {
            /** @var ClassMetadataInfo $metadata */
            list($metadata) = $metadataAndName;

            switch ($metadata->getTypeOfField($property)) {
                case Type::STRING:
                    // TODO: read annotation metadata to identify fields annotated as @Vich\UploadableField and similar definitions
                    if (preg_match('/image/i', $property)) {
                        return new TypeGuess('Vich\\UploaderBundle\\Form\\Type\\VichImageType', [], Guess::HIGH_CONFIDENCE);
                    }
                    if (preg_match('/file/i', $property) || preg_match('/upload/i', $property)) {
                        return new TypeGuess('Vich\\UploaderBundle\\Form\\Type\\VichFileType', [], Guess::HIGH_CONFIDENCE);
                    }
            }
        }

        return parent::guessType($class, $property);
    }
}
