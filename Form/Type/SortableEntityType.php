<?php

namespace C33s\Bundle\FormExtraBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType as BaseEntityType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\ChoiceList\DoctrineChoiceLoader;
use Symfony\Component\Form\ChoiceList\Factory\CachingFactoryDecorator;

class SortableEntityType extends BaseEntityType
{
    /**
     * @var DoctrineChoiceLoader[]
     */
    protected $choiceLoaders = array();

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $choiceLoader = function (Options $options) {
            // Unless the choices are given explicitly, load them on demand
            if (null === $options['choices']) {
                $hash = null;
                $qbParts = null;

                // If there is no QueryBuilder we can safely cache DoctrineChoiceLoader,
                // also if concrete Type can return important QueryBuilder parts to generate
                // hash key we go for it as well
                if (!$options['query_builder'] || false !== ($qbParts = $this->getQueryBuilderPartsForCachingHash($options['query_builder']))) {
                    $hash = CachingFactoryDecorator::generateHash(array(
                        $options['em'],
                        $options['class'],
                        $qbParts,
                        $options['order_by'],
                    ));

                    if (isset($this->choiceLoaders[$hash])) {
                        return $this->choiceLoaders[$hash];
                    }
                }

                if (null !== $options['query_builder']) {
                    $queryBuilder = $options['query_builder'];
                } else {
                    $queryBuilder = $options['em']->getRepository($options['class'])->createQueryBuilder('e');
                }
                $this->applyQueryBuilderSorting($queryBuilder, $options['order_by']);
                $entityLoader = $this->getLoader($options['em'], $queryBuilder, $options['class']);

//                if (null !== $options['query_builder']) {
//                    $entityLoader = $this->getLoader($options['em'], $options['query_builder'], $options['class'], $options['order_by_try_fields']);
//                } else {
//                    $queryBuilder = $options['em']->getRepository($options['class'])->createQueryBuilder('e');
//                    $entityLoader = $this->getLoader($options['em'], $queryBuilder, $options['class'], $options['order_by_try_fields']);
//                }

                $doctrineChoiceLoader = new DoctrineChoiceLoader(
                    $options['em'],
                    $options['class'],
                    $options['id_reader'],
                    $entityLoader
                );

                if (null !== $hash) {
                    $this->choiceLoaders[$hash] = $doctrineChoiceLoader;
                }

                return $doctrineChoiceLoader;
            }
        };

        $resolver->setDefaults(array(
            'order_by' => ['name' => 'asc', 'title' => 'asc'],
            'choice_loader' => $choiceLoader,
        ));

        $resolver->setNormalizer('order_by', function (Options $options, $value) {
            if (is_array($value)) {
                return $value;
            }
            if (is_string($value)) {
                return [$value => 'asc'];
            }

            return [];
        });
    }

    /**
     * Return the default loader object.
     *
     * @param ObjectManager $manager
     * @param QueryBuilder  $queryBuilder
     * @param string        $class
     *
     * @return ORMQueryBuilderLoader
     */
    public function getLoader(ObjectManager $manager, $queryBuilder, $class)
    {
        return new ORMQueryBuilderLoader($queryBuilder);
    }

    public function reset()
    {
        $this->choiceLoaders = array();
    }

    protected function applyQueryBuilderSorting(QueryBuilder $queryBuilder, array $orderBy)
    {
        $className = $queryBuilder->getRootEntities()[0];
        $alias = $queryBuilder->getRootAliases()[0];
        $em = $queryBuilder->getEntityManager();
        $metadata = $em->getClassMetadata($className);

        foreach ($orderBy as $name => $direction) {
            if ($metadata->hasField($name)) {
                $queryBuilder
                    ->orderBy($alias.'.'.$name, $direction)
                ;
            }
        }
    }
}
